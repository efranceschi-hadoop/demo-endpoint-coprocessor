# demo-endpoint-coprocessor

Projetos cliente e servidor para demonstrar o funcionamento da funcionalidade **Endpoint CoProcessor** do HBase. 

## Configurações necessárias

Adicionar a seguinte configuração em /etc/hbase/conf/hbase-site.xml:

```
  <property>
    <name>hbase.coprocessor.region.classes</name>
    <value>com.example.demo.ec.SumEndPoint</value>
  </property>
```