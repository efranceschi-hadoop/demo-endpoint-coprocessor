package com.example.demo.ec;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.client.coprocessor.Batch;
import org.apache.hadoop.hbase.io.compress.Compression;
import org.apache.hadoop.hbase.ipc.BlockingRpcCallback;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Eduardo Franceschi
 * @since 2019-04-03.
 */
public class ECClient {

	private static final String TABLE_NAME = "test_table";
	private static final String CF_INFO = "i";
	private static final String CQ_VALOR = "v";

	public static void main(String[] args) throws Throwable {
		new ECClient().execute();
	}

	private void execute() throws Throwable {
		Configuration config = hBaseConfiguration();
		try (Connection connection = ConnectionFactory.createConnection(config)) {
			createTable(connection);
			insertSample(connection);
			long start = System.currentTimeMillis();
			long sum = sum(connection);
			long duration = System.currentTimeMillis() - start;
			System.out.println("Sum is " + sum + " (" + duration + "ms)");
		}
	}

	private long sum(Connection connection) throws Throwable {
		Table table = connection.getTable(TableName.valueOf(TABLE_NAME));
		final Sum.SumRequest request = Sum.SumRequest.newBuilder().setFamily(CF_INFO).setColumn(CQ_VALOR).build();
		Map<byte[], Long> results = table.coprocessorService(Sum.SumService.class, null, null, new Batch.Call<Sum.SumService, Long>() {
			@Override
			public Long call(Sum.SumService aggregate) throws IOException {
				BlockingRpcCallback<Sum.SumResponse> rpcCallback = new BlockingRpcCallback();
				aggregate.getSum(null, request, rpcCallback);
				Sum.SumResponse response = rpcCallback.get();
				return response != null && response.hasSum() ? response.getSum() : 0L;
			}
		});
		Long total = 0L;
		for (Long partial : results.values()) {
			System.out.println("Partial Sum = " + partial);
			total += partial;
		}
		return total;
	}

	private void insertSample(Connection connection) throws IOException {
		Table table = connection.getTable(TableName.valueOf(TABLE_NAME));
		List<Put> list = new ArrayList<>();
		for (long i = 1; i <= 10000; i++) {
			Put put = new Put(Bytes.toBytes(i));
			put.addColumn(Bytes.toBytes(CF_INFO), Bytes.toBytes(CQ_VALOR), Bytes.toBytes(i));
			list.add(put);
		}
		table.put(list);
	}

	private void createTable(Connection connection) throws IOException {
		Admin admin = connection.getAdmin();
		if (!admin.tableExists(TableName.valueOf(TABLE_NAME))) {
			HTableDescriptor tableDescriptor = new HTableDescriptor(TABLE_NAME);
			HColumnDescriptor cd = new HColumnDescriptor(CF_INFO);
			cd.setCompressionType(Compression.Algorithm.SNAPPY);
			tableDescriptor.addFamily(cd);
			admin.createTable(tableDescriptor);
		}
	}

	private Configuration hBaseConfiguration() throws IOException, ServiceException {
		Configuration config = HBaseConfiguration.create();

		String path = Objects.requireNonNull(this.getClass()
				.getClassLoader()
				.getResource("hbase-site.xml"))
				.getPath();
		config.addResource(new Path(path));

		HBaseAdmin.checkHBaseAvailable(config);

		return config;
	}

}
